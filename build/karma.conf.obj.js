var paths = require('./paths');

module.exports = {

	// base path that will be used to resolve all patterns (eg. files, exclude)
	basePath: ".",

	// frameworks to use
	// available frameworks: https://npmjs.org/browse/keyword/karma-adapter
	frameworks: ['jasmine'],

	jspm: {
		config: 'test.config.js',
		loadFiles: [
			'built/unit/**/*.test.js'
		],
		serveFiles: [
			'built/!(unit)/*.js',
			'built/unit/**/*!(.test).js'
		]
	},

	// list of files / patterns to load in the browser
	files: [
		//'node_modules/babel-core/browser-polyfill.js'
		'jspm_packages/github/es-shims/es5-shim@4.3.0/es5-shim.js',
		'jspm_packages/github/es-shims/es6-shim@0.35.0/es6-shim.js',
		'jspm_packages/system.js',
		'config.js',
		paths.buildDir + '/**/*.js',
    	'jspm_packages\github\angular\bower-angular-mocks@1.5.6\angular-mocks.js',
        paths.testJS
	],

	// list of files to exclude
	exclude: [
	],

	plugins:[
		'karma-chrome-launcher',
		'karma-phantomjs-launcher',
		'karma-jasmine'
	],

	//// preprocess matching files before serving them to the browser
	//// available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
	preprocessors: {
		'test/**/*.ts': [],
		'src/**/*.ts': []
	},

	// test results reporter to use
	// possible values: 'dots', 'progress'
	// available reporters: https://npmjs.org/browse/keyword/karma-reporter
	reporters: ['progress'],

	// web server port
	port: 9876,

	// enable / disable colors in the output (reporters and logs)
	colors: true,

	// enable / disable watching file and executing tests whenever any file changes
	autoWatch: true,

	// start these browsers
	// available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
	browsers: ['PhantomJS'],

	// Continuous Integration mode
	// if true, Karma captures browsers, runs the tests and exits
	singleRun: false
};
