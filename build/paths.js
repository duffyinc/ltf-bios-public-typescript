var path = require('path');
var url = require('url');
var appRoot = 'src';
var testRoot = 'test';
var fontRoot = 'clientlibs/fonts';
var cssRoot = 'clientlibs/css';
var jsRoot = 'clientlibs/js';
var imageRoot = 'images';
var jspmRoot = 'jspm_packages';
var toolsRoot = 'tools';
var buildOutput = 'built';
var buildTemp = 'buildTemp';
var outputRoot = 'dist';
var user = 'cq5testing';
var password = 'Lif3t1M3';
var config = 'web.config';

function envPath() {
	var env = process.env.TEST_ENV;
	var baseUrl = process.env.BASE_URL;
	var testUrl;
	if(env === 'local') {
		testUrl = baseUrl;
	} else{
		if(env == null) {
			env = 'qa';
		}
		if(baseUrl == null) {
			baseUrl = 'mylt.lifetimefitness.com';
		}
		testUrl = env + '.' + baseUrl;
	}
	return testUrl;
}

module.exports = {
	root: path.resolve('.'),
	sourceDir: appRoot,
	source: path.join(appRoot, '**/*.js'),
	html: path.join(appRoot, '**/*.html'),
	componentHtml: path.join(appRoot, 'components/**/!(.tpl).html'),
	componentTemplates: path.join(appRoot, 'components/**/*.hbs'),
	partialHtml: path.join(appRoot,'lib','partials', '**/*.hbs'),
	partialHtmlDir: path.join(appRoot,'lib','partials'),
	indexHtml : path.join(appRoot, 'test-pages', 'member', 'member' + '.html'),
	json: path.join(appRoot, '**/*.json'),
	configFile: 'config.js',
	webConfigFile: 'web.config',
	testPageTemplates: path.join(appRoot, 'test-pages', '!(partials)/**/*.hbs'),
	testPagePartialTemplates: path.join(appRoot, 'test-pages', 'partials/**/*.hbs'),
	testPagePartialDir: path.join(appRoot, 'test-pages', 'partials'),
	testPageHtmlOut: path.join(appRoot, 'test-pages'),
	buildHtmlOut: path.join(outputRoot, 'src', 'htmlFiles'),
	srcJS: path.join(appRoot, '**/*.ts'),
	testJS: path.join(testRoot, '**/*.ts'),
	vendorRoot: path.join(appRoot, 'scripts'),
	unitTestIndex: path.join(testRoot, 'unit_test_index.js'),
	styles: path.join(appRoot, '**/*.scss'),
	stylesDir: path.join(appRoot, 'styles'),
	mainStylesheet: path.join(appRoot, 'styles/scss', 'member.scss'),
	fonts: path.join(appRoot, 'fonts/*/**'),
	images: path.join(imageRoot, '**/*'),
	jspm: path.join(jspmRoot, '**/*'),
	typings: path.join(toolsRoot, 'typings'),
	tsDefinitions: path.join(toolsRoot, 'typings', '**', '*.ts'),
	tsVendorDefinitions: path.join(toolsRoot, 'typings', 'tsd.d.ts'),
	appTypeScriptReferences: path.join(toolsRoot, 'typings', 'typescriptApp.d.ts'),
	buildDir: buildOutput,
	tempDir: buildTemp,
	dist: outputRoot,
	output: outputRoot,
	componentHtmlOut: path.join(outputRoot),
	fontOut: path.join(outputRoot, fontRoot),
	imageOut: path.join(outputRoot, imageRoot),
	jspmOut: path.join(outputRoot, 'jspm_packages'),
	cssOut: path.join(outputRoot, cssRoot),
	jsOut: path.join(outputRoot, jsRoot),
	sourceMapRelativePath: path.join('..', appRoot),
	e2eSpecsSrc: path.join(testRoot, 'e2e', appRoot),
	e2eSpecsSrcGlob: path.join(testRoot, 'e2e', appRoot, '**/*.js'),
	e2eSpecsDist: path.join(testRoot, 'e2e', outputRoot),
	cq5_username: process.env.CQ5_LOGIN || 'mylt44',
	cq5_password: process.env.CQ5_PWD || 'Password1',
	webConfigRoot: path.join(config),
	webConfigOut: path.join(outputRoot),
	getTestingUrl: function(path) {
		var protocol = process.env.TEST_ENV === 'local' ? 'http' : 'https';
		return url.resolve(protocol + '://' + user + ":" + password + "@" + envPath(), path);
	}
};
