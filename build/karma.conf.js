// Karma configuration

var karmaConfig = require('./karma.conf.obj.js');

module.exports = function(config) {
	// level of logging
	// possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
	karmaConfig.logLevel = config.LOG_INFO;
    config.set(karmaConfig);
};
