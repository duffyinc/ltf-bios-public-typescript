exports.config = {
	directConnect: true,

	// Capabilities to be passed to the webdriver instance.
	capabilities: {
		'browserName': 'chrome'
	},

	//seleniumAddress: 'http://0.0.0.0:4444',
	// add proper version number
	seleniumServerJar: './node_modules/gulp-protractor/node_modules/protractor/selenium/selenium-server-standalone-2.44.0.jar',
	specs: ['test/e2e/dist/*.js'],

	plugins: [{
		path: 'ltf.protractor.js'
	}],


	onPrepare: function(){
		var SpecReporter = require('jasmine-spec-reporter');
		jasmine.getEnv().addReporter(new SpecReporter({displayStacktrace: true}));
		global.drv = browser.driver;
		global.paths = require('./paths');
	},
	framework: 'jasmine2',
	// Options to be passed to Jasmine-node.
	jasmineNodeOpts: {
		showColors: true,
		defaultTimeoutInterval: 300000,
		print: function(){},
		grep: process.env.SPEC_GREP
	}
};