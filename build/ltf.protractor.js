/* Ltf Components Protractor Plugin */
function addValueBindLocator() {
  by.addLocator('valueBind', function (bindingModel, opt_parentElement) {
    var using = opt_parentElement || document;
    var matches = using.querySelectorAll('*[value\\.bind="' + bindingModel +'"]');
    var result;

    if (matches.length === 0) {
      result = null;
    } else if (matches.length === 1) {
      result = matches[0];
    } else {
      result = matches;
    }

    return result;
  });
}

function loadAndWaitForLtfComponents(pageUrl) {
  browser.get(pageUrl);
  return browser.executeAsyncScript(
    'var cb = arguments[arguments.length - 1];' +
    'document.addEventListener("ltf-components-composed", function (e) {' +
    '  cb("Ltf Components composed")' +
    '}, false);'
  ).then(function(result){
      console.log(result);
      return result;
  });
}

function waitForHttpDone() {
  return browser.executeAsyncScript(
    'var cb = arguments[arguments.length - 1];' +
    'document.addEventListener("ltf-http-client-requests-drained", function (e) {' +
    '  cb(true)' +
    '}, false);'
  ).then(function(result){
      return result;
  });
}

/* Plugin hooks */
exports.setup = function(config) {
  // Ignore the default Angular synchronization helpers
  browser.ignoreSynchronization = true;

  // add the aurelia specific valueBind locator
  addValueBindLocator();

  // attach a new way to browser.get a page and wait for Ltf Components to complete loading
  browser.loadAndWaitForAureliaPage = loadAndWaitForLtfComponents;

  // wait for all http requests to finish
  browser.waitForHttpDone = waitForHttpDone;
};

exports.teardown = function(config) {};
exports.postResults = function(config) {};