var gulp = require('gulp'),
	gutil = require('gulp-util'),
	changed = require('gulp-changed'),
	path = require('path'),
	paths = require('../paths.js'),
	rename = require('gulp-rename'),
	template = require('gulp-template'),
	systemConfig = path.join(paths.root, 'config.js'),
	systemConfigMapper = require('./lib/system-config-mapper'),
	karma = require('karma');

gulp.task('copy-test-config', function(done) {
	var sysConfig = systemConfigMapper.obj(systemConfig);

	sysConfig.baseURL = '';

	if(sysConfig.paths && sysConfig.paths['github:components/jquery*']) {
		delete sysConfig.paths['github:components/jquery*'];
	}

	gulp.src('test.config.template')
		.pipe(changed(paths.root))
		.pipe(template({obj:sysConfig}))
		.pipe(rename('test.config.js'))
		.pipe(gulp.dest(paths.root))
		.on('end', done);
});

gulp.task('run-karma', ['copy-test-config', 'compile-ts', 'compile-test-ts'], function(done){
	var	karmaConfig = require('../karma.conf.obj.js');

	karmaConfig.autoWatch = false;
	karmaConfig.singleRun = true;

	karma.server.start(karmaConfig, function(err){
		//if(err || !process.env.NODE_ENV === 'local') throw new gutil.PluginError('karma', err);

		done();
	});
});

gulp.task('run-karma-ci', ['copy-test-config', 'compile-ts', 'compile-test-ts'], function(done) {
	var	config = require('../karma.conf.obj.js');

	config.autoWatch = false;
	config.singleRun = true;
	config.plugins.push('karma-junit-sonarqube-reporter');
	config.plugins.push('karma-coverage');
	config.reporters.push('junit');
	config.reporters.push('coverage');
	config.preprocessors['src/**/!(build).js'].push('coverage');

	config.junitReporter = {
		outputFile: 'reports/junit/test-results.xml',
		suite: ''
	};

	config.coverageReporter = {
		type:   'lcov',
		dir:    'reports',
		subdir: 'coverage'
	};

	karma.server.start(config, function(err) {
		if(err) throw new gutil.PluginError('karma', err);

		done();
	});
});
