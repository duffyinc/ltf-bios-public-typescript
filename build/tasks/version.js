var gulp = require('gulp'),
	gutil = require('gulp-util'),
	bump = require('gulp-bump'),
	git = require('gulp-git'),
	template = require('gulp-template'),
	paths = require('../paths.js'),
	runSequence = require('run-sequence'),
	fs = require('fs');


gulp.task('bump-version', function() {
	return gulp.src(['./package.json'])
		.pipe(bump({type: "prerelease"}).on('error', gutil.log))
		.pipe(gulp.dest('./'));
});

gulp.task('commit-changes', function() {
	gutil.log('committing changes');
	return gulp.src('.')
		.pipe(git.add())
		.pipe(git.commit('[maven-release-plugin] BAMBOO version bump.'));
});

gulp.task('push-changes', function(cb) {
	gutil.log('pushing changes');
	git.push('origin', 'develop', cb);
});

gulp.task('template-version-properties', function() {
	// not using require() because it'll load the cached version with the old version number (if previously bumped)
	var pkg = JSON.parse(fs.readFileSync('package.json', 'utf8'));

	gulp.src('version.properties')
		.pipe(template({version: pkg.version}))
		.pipe(gulp.dest(paths.dist));
});

gulp.task('version', function(cb) {
	runSequence(
		'bump-version',
		'template-version-properties',
		'commit-changes',
		'push-changes',
		function(err) {
			if(err) {
				console.log(err);
			}else {
				console.log('Version bump complete successfully.')
			}
			cb(err);
		});
});
