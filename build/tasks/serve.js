var gulp = require('gulp'),
	paths = require('../paths'),
	rename = require('gulp-rename'),
	spawn = require('cross-spawn'),
	browserSync = require('browser-sync').create();

gulp.task('serve', ['app-bundles', 'serve-styles', 'copy-images', 'copy-fonts', 'copy-content', 'copy-html', 'lint-js-watch', 'watch-no-test'], function() {
	require('../../server');

	browserSync.init({
		open: 'local',
		browser: 'google chrome',
		proxy: 'http://localhost:1337'
	});

	require('../../server');

	gulp.watch(paths.srcJS, ['serve-js']);
	gulp.watch(paths.styles, ['serve-styles']);
	gulp.watch([paths.componentTemplates, paths.partialHtml, paths.testPageTemplates, paths.testPagePartialTemplates], ['serve-html']);
});

// These wrapper tasks ensure reload isn't called until the parent tasks finish.
// The reload call is wrapped in a function because otherwise gulp thinks the wrapper tasks never finish.
gulp.task('serve-js', ['check-types'], function() { browserSync.reload(); } );
gulp.task('serve-html', ['copy-html'], function() { browserSync.reload(); } );

// Serve-styles is different than the other serves because styles can be auto-injected, through stream(),
// instead of requiring a full reload.
var sass = require('gulp-sass'),
	browserSupportQuery = ['ie >= 11', 'last 2 versions'],
	vmin = require('postcss-vmin'),
	doiuse = require('doiuse'),
	epub = require('postcss-epub'),
	imageset = require('postcss-image-set'),
	postcss = require('gulp-postcss'),
	sourcemaps = require('gulp-sourcemaps'),
	autoprefixer = require('autoprefixer-core'),
	postcssProcessors = function() {
		var processors = [
			imageset(),
			autoprefixer({
				browsers: browserSupportQuery
			}),
			epub()
		];

		return processors;
	};

gulp.task('serve-styles', ['template-dependencies'], function() {
	gulp.src(paths.mainStylesheet)
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(postcss(postcssProcessors()))
		.pipe(rename({ suffix: '.min'}))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(paths.cssOut))
		.pipe(browserSync.stream());
});
