var gulp = require('gulp'),
	gutil = require('gulp-util'),
	debug = require('gulp-debug'),
	changed = require('gulp-changed'),
	cached = require('gulp-cached'),
	remember = require('gulp-remember'),
	plumber = require('gulp-plumber'),
	path = require('path'),
	paths = require('../paths'),
	scsslint = require('gulp-scss-lint'),
	scssStylish = require('gulp-scss-lint-stylish'),
	failBuild = false,
	runSequence = require('run-sequence'),
	tslint = require('gulp-tslint'),
	tslintStylish = require('gulp-tslint-stylish'),
	watch = require('gulp-watch');

gulp.task('fail-on-lint-error', function(){
	failBuild = true;
});

gulp.task('lint', ['lint-js', 'lint-styles']);

gulp.task('lint-js', function(){
	var filesToLint = [paths.srcJS, paths.testJS];

	return gulp.src(filesToLint)
		.pipe(cached('js'))
		.pipe(tslint())
		.pipe(tslint.report(tslintStylish, {
			emitError: failBuild,
			sort: true,
			bell: true
		}));
});

gulp.task('lint-js-watch', function(){
	var filesToLint = [paths.srcJS, paths.testJS];
	
	watch(filesToLint)
		.pipe(tslint())
		.pipe(tslint.report(tslintStylish, {
			emitError: true,
			sort: true,
			bell: true
		}));
});

gulp.task('lint-styles', function(){
	gulp.src([paths.styles, '!src/styles/scss/reset/_normalize.scss', '!src/styles/scss/reset/_print.scss'])
		.pipe(scsslint({
			config: 'scss-lint.yml',
			customReport: scssStylish
		}))
		.pipe(scsslint.failReporter('E'));
});
