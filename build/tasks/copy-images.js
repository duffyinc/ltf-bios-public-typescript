var gulp = require('gulp'),
	changed = require('gulp-changed'),
	path = require('path'),
	paths = require('../paths');

gulp.task('copy-images', function(){
	gulp.src(paths.images)
		.pipe(changed(paths.imageOut))
		.pipe(gulp.dest(path.join(paths.imageOut)));
});
