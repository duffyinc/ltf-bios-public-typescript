var gulp = require('gulp'),
	rename = require('gulp-rename'),
	sass = require('gulp-sass'),
	postcss = require('gulp-postcss'),
	autoprefixer = require('autoprefixer-core'),
	cssnano = require('cssnano'),
	vmin = require('postcss-vmin'),
	doiuse = require('doiuse'),
	epub = require('postcss-epub'),
	imageset = require('postcss-image-set'),
	sourcemaps = require('gulp-sourcemaps'),
	path = require('path'),
	paths = require('../paths'),
//https://github.com/ai/browserslist#queries for how to write the query
	browserSupportQuery = ['ie >= 9', 'last 2 versions'],
	testBrowsers = false;

var output = paths.cssOut,
	postcssProcessors = function(validateBrowsers) {
		var processors = [
			imageset(),
			vmin(),
			autoprefixer({
				browsers: browserSupportQuery
			}),
			epub()
		];

		if(validateBrowsers) {
			processors.push(doiuse({
				browsers: browserSupportQuery,
				onFeatureUsage: function(usageInfo) {
					console.log(usageInfo.message);
				}
			}));
		}

		processors.push(cssnano());

		return processors;
	};

gulp.task('styles', ['styles-post', 'copy-images']);

gulp.task('test-browsers', function() {
	testBrowsers = true;
});

gulp.task('browser-style-support', ['test-browsers', 'styles']);

gulp.task('styles-post', ['styles-pre'], function() {
	//post-process here
});

gulp.task('styles-pre', function(done) {

	gulp.src(paths.mainStylesheet)
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(postcss(postcssProcessors(testBrowsers)))
		.pipe(rename({ suffix: '.min'}))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(output))
		.on('end', function() {
			done();
		});
});
