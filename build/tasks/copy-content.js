var gulp = require('gulp'),
	changed = require('gulp-changed'),
	path = require('path'),
	paths = require('../paths');

gulp.task('copy-content', function(){
    
var jspmPackages = gulp.src(paths.jspm)
		.pipe(changed(paths.jspmOut))
		.pipe(gulp.dest(path.join(paths.jspmOut)));
        
var configFile = gulp.src(paths.configFile)
		.pipe(gulp.dest(paths.dist));
		
var copySrc = gulp.src(paths.srcJS)
		.pipe(gulp.dest(path.join(paths.dist, 'src')));
		
var copyStatesJson = gulp.src(paths.json)
		.pipe(gulp.dest(function(file){
			return path.join(paths.dist, path.basename(file.base));
		}))
});
