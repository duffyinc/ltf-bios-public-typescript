var gulp = require('gulp'),
	changed = require('gulp-changed'),
	path = require('path'),
	paths = require('../paths');

gulp.task('copy-fonts', function(){
	gulp.src(paths.fonts)
		.pipe(changed(paths.fontOut))
		.pipe(gulp.dest(path.join(paths.fontOut)));
});
