var gulp = require('gulp'),
	changed = require('gulp-changed'),
	path = require('path'),
	paths = require('../paths');

gulp.task('copy-config', function(){
	gulp.src(paths.webConfigRoot)
		.pipe(gulp.dest(path.join(paths.webConfigOut)));
});
