//get the config for System to map to package without knowing the version
var config = {};

global.System = {
	config: function(data) {
		config = data;
	}
};

module.exports = {
	map: function(configPath, key) {

		require(configPath);

		var mappedPath = config.map[key];

		var mapSplit = mappedPath.split(':');
		var directory = mapSplit[1];
		var directoryPattern = config.paths[mapSplit[0] + ":*"];

		return directoryPattern.replace('*', directory);
	},
	prop: function(configPath, propName) {
		require(configPath);

		return config[propName];
	},
	obj: function(configPath) {
		require(configPath);

		return config;
	}
};
