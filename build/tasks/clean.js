var gulp = require('gulp'),
	clean = require('del'),
	path = require('path'),
	glob = require('glob'),
	fs = require('fs'),
	paths = require('../paths');

gulp.task('clean', function(){
	clean(paths.dist);
});

gulp.task('clean-temp', function(){
	clean(paths.buildDir);
	clean(paths.tempDir);
});
