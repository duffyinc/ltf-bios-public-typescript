var gulp = require('gulp'),
	debug = require('gulp-debug'),
	plumber = require('gulp-plumber'),
	path = require('path'),
	changed = require('gulp-changed'),
	paths = require('../paths'),
	handlebars = require('gulp-compile-handlebars'),
	data = require('gulp-data'),
	rename = require('gulp-rename'),
    fs = require('fs'),
	merge = require('merge-stream');

gulp.task('copy-html', function() {
	var out = path.join(paths.componentHtmlOut);
	var testOut = paths.testPageHtmlOut;
	var htmlOut = path.join(paths.buildHtmlOut);
	var indexOut = path.join(paths.dist);
	var options = {
		batch: [paths.partialHtmlDir]
	};


	handlebars.Handlebars.registerHelper('get-date', function(options){
		return Math.floor(Date.now() / 1000);
	});

	handlebars.Handlebars.registerHelper('raw-helper', function(options){
		return options.fn();
	});

	handlebars.Handlebars.registerHelper('underscore', function(str) {
		return str.toLowerCase().replace(/ /g, '_');
	});

	var componentHtml = gulp.src(paths.componentHtml)
		.pipe(gulp.dest(function(file) {
			return path.join(out, path.basename(file.base));
		}));

	var completeHtml = gulp.src(paths.testPageTemplates)
		.pipe(data(function(file) {
			var baseName = path.basename(file.path, '.hbs');
			var partialFile = path.join(paths.sourceDir, 'components', baseName, baseName + '.hbs');
			var partialPath = path.resolve(partialFile);

			if(fs.existsSync(partialFile)) {
				var partial = fs.readFileSync(partialPath).toString();
				handlebars.Handlebars.registerPartial(baseName, partial);
			}

			return {
				componentName: baseName
			};
		}))
		.pipe(handlebars({}, {
			batch: [paths.testPagePartialDir]
		}))
		.pipe(rename({
			extname: '.html'
		}))
		.pipe(gulp.dest(htmlOut));

	var indexHtml = gulp.src(paths.indexHtml)
		.pipe(rename('index.html'))
		.pipe(gulp.dest(indexOut));

	var testPageTemplates = gulp.src(paths.testPageTemplates)
		.pipe(data(function(file) {
			var baseName = path.basename(file.path, '.hbs');
			var partialFile = path.join(paths.sourceDir, 'components', baseName, baseName + '.hbs');
			var partialPath = path.resolve(partialFile);

			if(fs.existsSync(partialFile)) {
				var partial = fs.readFileSync(partialPath).toString();
				handlebars.Handlebars.registerPartial(baseName, partial);
			}

			return {
				componentName: baseName
			};
		}))
		.pipe(handlebars({}, {
			batch: [paths.testPagePartialDir]
		}))
		.pipe(rename({
			extname: '.html'
		}))
		.pipe(gulp.dest(testOut));

	var componentTemplates = gulp.src(paths.componentTemplates)
		.pipe(data(function(file) {
			return {
				componentName: path.basename(file.path, '.hbs')
			};
		}))
		.pipe(handlebars({}, options))
		.pipe(rename({
			extname: '.html'
		}))
		.pipe(gulp.dest(function(file) {
			return path.join(out, path.basename(file.base));
		}));

	return merge(componentHtml, testPageTemplates, componentTemplates);
});
