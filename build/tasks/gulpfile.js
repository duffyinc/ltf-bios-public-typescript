var gulp = require('gulp'),
	karma = require('karma'),
	moment = require('moment'),
	runSequence = require('run-sequence'),
	paths = require('../paths');

gulp.task('watch-test', function() {
	console.log('watching and running tests...');
	process.env.NODE_ENV = 'local';
	runSequence(['styles', 'compile-ts', 'compile-test-ts']);

	gulp.watch(paths.srcJS, ['lint-js', 'run-karma']);
	gulp.watch(paths.testJS, ['lint-js', 'run-karma']);
	gulp.watch(paths.styles, ['lint-styles']);

});

gulp.task('watch-no-test', ['styles', 'compile-ts', 'compile-test-ts'], function() {
	console.log('watching and not running tests...');
	process.env.NODE_ENV = 'local';
	gulp.start('styles');

	gulp.watch(paths.srcJS, ['lint-js', 'compile-ts', 'copy-content', 'lint-js-watch']);
	gulp.watch(paths.testJS, ['lint-js', 'compile-test-ts']);
	gulp.watch(paths.styles, ['lint-styles', 'serve-styles']);

});

var start;

gulp.task('time-start', function(){
	start = moment();
});

gulp.task('time-end', function() {
	console.log(start.diff(moment(), 'seconds'));
});

gulp.task('set-debug', function() {
	global.debug = true;
});

gulp.task('app-bundles-debug', ['set-debug'], function() {
	runSequence('app-bundles');
});

gulp.task('checked-build', ['clean'/*, 'fail-on-lint-error'*/], function() {
	runSequence('time-start', 'template-version-properties', 'template-dependencies', 'lint', 'compile-ts', ['copy-html', 'app-bundles', 'styles'], 'time-end');
});

gulp.task('build-content', ['clean'/*, 'fail-on-lint-error'*/], function() {
	runSequence('app-bundles', 'copy-config', 'copy-fonts', 'serve-styles', 'copy-images','copy-content', 'copy-html');
});
gulp.task('test-build', ['checked-build'], function(){
	runSequence('run-karma');
});

gulp.task('test', ['lint', 'run-karma']);
gulp.task('build-deploy', ['build-content'] );
gulp.task('build', ['checked-build']);
gulp.task('watch', ['build', 'watch-test']);
gulp.task('watch-no-tests', ['watch-no-test', 'serve']);
gulp.task('build-local', function() {
	process.env.NODE_ENV = 'local';
	runSequence('checked-build');
});

gulp.task('default', ['test-build']);
