var pconfig = {};
try{
	pconfig = require('protractor/config.json');
}catch(e){
	pconfig.webdriverVersions = {
		selenium: '0.0.0'
	};
}

if(pconfig.webdriverVersions.selenium !== '2.43.1') {
	pconfig.webdriverVersions.selenium = '2.43.1';

	require('fs').writeFile(
		'node_modules/protractor/config.json', JSON.stringify(pconfig)
	);
}

var gulp = require('gulp');
var paths = require('../paths');
//var babel = require('gulp-babel');
var plumber = require('gulp-plumber');
var webdriver_update = require('gulp-protractor').webdriver_update;
var protractor = require('gulp-protractor').protractor;
var sourcemaps = require('gulp-sourcemaps');
var debug = require('gulp-debug');
var minimist = require('minimist');

var knownOptions = {
	string: 'grep',
	default:{ grep: '' }
};

var specOptions = minimist(process.argv.slice(2), knownOptions);
process.env.SPEC_GREP = specOptions.grep;
process.env.TEST_ENV = 'local';
process.env.BASE_URL = 'localhost:4503';
process.env.CQ5_LOGIN = 'mylt44';
process.env.CQ5_PWD = 'Password1';

// for full documentation of gulp-protractor,
// please check https://github.com/mllrsohn/gulp-protractor
gulp.task('webdriver_update', webdriver_update);

// transpiles files in
// /test/e2e/src/ from es6 to es5
// then copies them to test/e2e/dist/
gulp.task('build-e2e', function () {
	return gulp.src(paths.e2eSpecsSrcGlob)
		.pipe(sourcemaps.init())
		.pipe(plumber())
		.pipe(babel())
		.pipe(sourcemaps.write('.',{sourceRoot: paths.e2eSpecsSrc}))
		.pipe(gulp.dest(paths.e2eSpecsDist));
});

// runs build-e2e task
// then runs end to end tasks
// using Protractor: http://angular.github.io/protractor/
gulp.task('e2e', ['webdriver_update', 'build-e2e'], function(cb) {
	console.log('including tests matching: ' + process.env.SPEC_GREP);
	return gulp.src(paths.e2eSpecsDist + "*.js")
		.pipe(protractor({
			configFile: "protractor.conf.js",
			debug: false,
			args: ['--baseUrl', 'http://127.0.0.1:9000']
		}))
		.on('error', function(e) { console.log(e);throw e; });
});

gulp.task('watch-e2e', ['build-e2e'], function(){
	gulp.watch(paths.e2eSpecsSrcGlob,['build-e2e']).on('change', function(evt){
		console.log('File ' + evt.path + ' was ' + evt.type + ', rebuilding...');
	});
});

gulp.task('dev', function(){
	process.env.TEST_ENV = 'dev';
	process.env.BASE_URL = 'lifetimefitness.com';
	process.env.CQ5_LOGIN = 'mylt44';
	process.env.CQ5_PWD = 'Password1';
});


gulp.task('qa', function(){
	process.env.TEST_ENV = 'qa';
	process.env.BASE_URL = 'lifetimefitness.com';
	process.env.CQ5_LOGIN = 'mylt44';
	process.env.CQ5_PWD = 'Password1';
});

gulp.task('stage', function(){
	process.env.TEST_ENV = 'stage';
	process.env.BASE_URL = 'lifetimefitness.com';
	process.env.CQ5_LOGIN = 'stgmylt44';
	process.env.CQ5_PWD = 'Password1';
});

gulp.task('prod', function(){
	process.env.TEST_ENV = 'www';
	process.env.BASE_URL = 'lifetimefitness.com';
	process.env.CQ5_LOGIN = 'cantolick';
	process.env.CQ5_PWD = 'RPfFs8lTjFzpbf';
});

gulp.task('smoke-qa',['qa', 'e2e']);

gulp.task('smoke-stage',['stage', 'e2e']);

gulp.task('smoke-prod',['prod', 'e2e']);

gulp.task('smoke-dev', ['dev', 'e2e']);

gulp.task('smoke-local', ['e2e']);
