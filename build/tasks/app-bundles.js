var gulp = require('gulp'),
	gutil = require('gulp-util'),
	path = require('path'),
	paths = require('../paths.js'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	Builder = require("jspm").Builder,
	appFiles = [
		path.join(paths.vendorRoot, 'jquery.min.js'),
		path.join(paths.vendorRoot, 'lodash.min.js'),
		path.join(paths.vendorRoot, 'bootstrap.min.js')
	];

gulp.task('app-bundles', function() {
	var builder = new Builder("../../", {
    typeScriptOptions: {
      "module": "system",
      "sourceMap": true
    },
    map: {
      "ts": "github:frankwallis/plugin-typescript@4.0.16"
    }
	});

	gulp.src(appFiles)
     .pipe(concat('vendor.min.js'))
     .pipe(uglify())
     .pipe(gulp.dest(paths.jsOut));

	builder.buildStatic("./src/components/member/bootstrap.ts", "./dist/clientlibs/js/app.min.js", { minify: true, mangle: false });
});
