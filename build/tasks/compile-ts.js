var gulp = require('gulp'),
	debug = require('gulp-debug'),
	changed = require('gulp-changed'),
	cached = require('gulp-cached'),
	remember = require('gulp-remember'),
	rename = require('gulp-rename'),
	runSequence = require('run-sequence'),
	ts = require('gulp-typescript'),
	paths = require('../paths.js'),
	sourcemap = require('gulp-sourcemaps'),
	tsconfig = require('../../tsconfig.json');

function tsCompile(src, compilerOptions, out, done, srcMap) {
	var stream = gulp.src(src);//[src, paths.tsDefinitions]);
	var tempBuildDir = 'buildTemp';

	srcMap = srcMap || true;
	out = out || false;
	done = done || null;

	if(srcMap) {
		stream = stream.pipe(sourcemap.init());
	}

	if(out) {
		stream = stream.pipe(cached('ts')).pipe(changed(tempBuildDir)).pipe(gulp.dest(tempBuildDir));
	}

	stream = stream.pipe(ts(compilerOptions));

	if(srcMap) {
		stream = stream.pipe(sourcemap.write('.'));
	}

	if(out) {
		stream.pipe(gulp.dest(out));
	}

	if(done) {
		stream.on('end', done);
	}
}

gulp.task('compile-ts', function(done) {
	tsCompile(paths.srcJS, tsconfig.compilerOptions, paths.buildDir, done);
});

gulp.task('compile-test-ts', function(done) {
	tsCompile(paths.testJS, tsconfig.compilerOptions, paths.buildDir, done);
});

gulp.task('check-types', function() {
	tsCompile(paths.srcJS, tsconfig.compilerOptions);
});

gulp.task('compile', ['compile-ts', 'compile-test-ts']);
