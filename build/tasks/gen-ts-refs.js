var gulp = require('gulp'),
	inject = require('gulp-inject'),
	paths = require('../paths.js');


gulp.task('gen-ts-refs', function() {
	var target = gulp.src(paths.appTypeScriptReferences);
	var sources = gulp.src([paths.srcJS], {read: false});
	return target.pipe(inject(sources, {
		starttag: '//{',
		endtag: '//}',
		transform: function(filepath) {
			return '/// <reference path="../..' + filepath + '" />';
		}
	})).pipe(gulp.dest(paths.typings));
});
