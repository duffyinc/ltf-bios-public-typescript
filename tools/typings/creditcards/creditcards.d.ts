import cc = CreditCards;

declare module CreditCards {
	interface ICreditCards {
		card: {
			isValid(number: string, type: string): boolean;
			type(number: string): string;
			type(number: string, eager: boolean): string;
			luhn(number: string): boolean;
		}
	}
}

declare module 'CreditCards' {
	var CreditCards: cc.ICreditCards;
	export default CreditCards;
}
