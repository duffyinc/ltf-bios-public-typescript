declare module Reflux {
	interface RefluxStatic {
		createStore<T>(store: StoreInitializer): T;
		createAction: Object;
	}

	interface StoreInitializer {
		init?(): Function;
	}

	interface Store {
		getInitialData?(): any;
		listen(callback: (data: any) => void): Function;
	}

}

declare module 'reflux' {
	var Reflux: Reflux.RefluxStatic;
	export default Reflux;
}
