/// <reference path="Reflux.d.ts" />

import lt = LtFramework;

interface Window {
	lt: {
		globalPageDrivers: {
			programRegistration: {
				[key: string]: string
			},
			global: {
				[key: string]: string
			}
		},
		api: {
			programRegistration: string
		}
	};
}


declare module LtFramework {
	export interface Profile {
		sso: string;
		partyId: number;
		token: string,
		personalization: { [key:string]: Object},
		memberDetails: Object
	}

	interface NavigationEventResults {
		didFail: boolean;
		error: string,
		destination: string,
		parameters: Object
	}

	interface NavigationEventCallback{
		(results: NavigationEventResults) : void
	}

	interface Actions {
		requestNavigation(key: string, parameters: Object, targetBlank: boolean): void;
	}

	interface Static {
		global: Window;
		actions: Actions;
		getStore(name: string): Reflux.Store;
		mountTo(obj: Object): void;
	}

	interface ProfileStore extends Reflux.Store {
		getInitialData(): Profile;
	}

	interface NavigationEventStore extends Reflux.Store {
		getInitialData(): NavigationEventResults;
		listen(callback: NavigationEventCallback): Function;
	}

	interface MockStore<T> extends Reflux.Store {
		simulateChange(data: T): void;
	}

	interface MockProfileStore extends MockStore<Profile> {

	}

	interface MockNavigationEventStore extends MockStore<NavigationEventResults> {
		setValidDestinations(destinations: Object): void;
		simulateNavigationRequest(destination: string, params: Object, targetBlank: boolean): void;
	}

}

declare module 'LtFramework' {
	var LtFramework: lt.Static;
	export default LtFramework;
}
