/// <reference path="jquery/jquery.d.ts" />

declare var jquery: JQueryStatic;
declare var $: JQueryStatic;

export default jquery;
