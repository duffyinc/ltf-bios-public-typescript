#[Lifetime Fitness Totalhealth-Program Registration Front End Assets](https://stash.ltfinc.net/projects/TH/repos/programregistration/browse)
**This document is a work in progress, if you find anything out of place, either fix it and submit a pull request, or
create an issue for it**

This repository has the Preprocessor Stylesheets and JavaScript files.  It also has the node infrastructure to
 transpile, process, test, and locally serve pages.


## Javascript

 The javascript platform is built using jquery using ecmascript 2015 standard specification.  Nodejs is 
 leveraged to provide code linting, test runners, transpilation, bundling, and a development server.
 
### Frameworks and Libraries
 
 - **NodeJs** - development platform
 - **ReluxJs** - a simple flux implementation to allow for one-way data flow through pub/sub data stores and actions
 - **jQuery** - Ajax in the browser, available for usage of third party behavior plugins
 - **moment.js** - javascript date manipulation made simple
 - **gulp** - development task runner
 - **jspm** - development module loader, processor, bundler
 - **typescript** - staticly typed javascript transpilation
 - **karma** - cross browser unit test runner
 - **jasmine** - unit testing framework
 - **protractor** - selenium chrome driver based automated functional testing framework

### Resources

- [RefluxJs Repo] (https://github.com/spoike/refluxjs)
- [jQuery Documentation](http://api.jquery.com/)
- [Moment.js Documentation](http://momentjs.com/docs/) and [Timezone Usage](http://momentjs.com/timezone/docs/)
- [Learn es2015 from Babel](http://babeljs.io/docs/learn-es2015/) - You can find babel configuration here as well.
- [Typescript Documentation and Resources] (http://www.typescriptlang.org/)
- [Jasmine Documentation](http://jasmine.github.io/2.0/introduction.html)
- [Gulp Documentation](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md)
- [Protractor API Documentation](https://angular.github.io/protractor/#/api)
- [JSPM Website](http://jspm.io)
- [JSPM Getting Started](https://github.com/jspm/jspm-cli/blob/master/docs/getting-started.md)

### Folder Structure

The folder structure will be split at the top level based on functional use of the containing files, i.e. source code, 
test code, etc.

    repo-folder/ - contains base configuration files used by tooling, this readme, and the dev server file --
      |
      build/ - contains build configuration files
      |   |
      |   tasks/ - contains gulp tasks
      |
      images/ - any image content used by documentation or test pages.
      |
      node_modules/ - conventional home for local node modules, **managed by npm and ignored by git**
      |
      jspm_packages/ - conventional home for packages, **managed by jspm and ignored by git**
      |
      src/ - root of application source code
      |   |
      |   components/ - ui component modules 
      |   |
      |   lib/ - reusable code modules that have no ui, i.e. logical services
      |   |
      |   test-pages/ - Devlopment time test pages
      |   |
      |   vendor/ - shims for external code libraries like jquery or lodash that can be broken out if necessary
      |
      test/ - root of test code
          |
          lib/ - helper library for test code
          |
          e2e/ - functional end to end tests for use with protractor
          |
          unit/ - unit tests

### Module Organization

Source code will be organized and composed based on a module system.  Since we are using ecmascript 2015+ with the help 
of Typescript, we are able to abstract away what the low level modules look like to the browser and use the standard 
compliant module system.  Where the functional modules live are annotated in the folder structure.

#### Guidelines

The page level module should pull all necessary higher order ui components in and orchestrate them as needed for the page 
and is responsible for orchestrating the data the flows through them by using modules from the lib.  This is also the 
place to define any HTML fragments that will need to be imported or defined in an external container in order for the 
bundle to attach its ui elements.

The component modules should be able to be rendered to any block element on a page. Any sub components that will not 
be reused outside of the component should be defined as sub folders within the component folder and be imported by way 
of relative pathing.  If a component is intended to be used a standalone external component, it may also necessary to 
define an HTML fragment that will be imported in order to attach the rendered component to the dom.
 
The lib folder should contain any code libraries such as model definitions, business logic services, or rest service 
wrappers that can be reused in other components.

When defining a module, a folder should be added under the appropriate folder with the name of the module and an 
`index.js` file placed at the root of that new folder.  Using module loading conventions, `index.js` is the default 
entry point of a module.  In order to avoid knowing pathing within any module, configurations will be in place to be 
able to import a module rooted from the `src` folder, `node_modules`, or the `test/lib` folder.  When importing a 
module, from any of the root folders, you only need to know the relative path, i.e.:

```
import moduleName from 'components/ModuleName'
```
This will load src/components/ModuleName/index.js.

With this system, we don't need to know external path structure and can avoid using `../../../../components/name` in our 
module imports.  It should be noted, with this method we leave off the `.js` from the module name.

JSPM loads all javascript code and uses the javascript modules to detect dependencies and properly bundle required 
files together.  The resolution root folders for module imports will be configured in the `config.js` file associated 
with the environment.

### Dev Workflow

#### Getting Started

   1. It all starts with Nodejs.  Install Node <https://nodejs.org>
   2. Next, to configure authentication with our internal NPM repo (https://nexus.ltfinc.net), create a file called ".npmrc" in <USER_HOME> (C:\\Users\\<username> on Windows) and put the following in it:

          _auth=<username:password converted to base64>
          email=<your @lifetimefitness.com email address>
      This process will get a lot easier once our Nexus is upgraded to 3.0 -- we'll be able to use `npm login` then.
   3. Now, open up a commandline and navigate to the root directory of this repository (make sure you're on the develop branch!).
   4. Execute the command: `npm install`
      * if you have visual studio install add the following parameter: `--msvs_version=<yourVersion>`
      * this will install all the node modules and you should see a `node_modules` folder now.
      * this will also install jspm and gulp as global node modules
   5. Execute the commands `npm install jspm -g` and `npm install gulp -g`
      * this will install jspm and gulp as global modules
   5. Execute the command: `jspm install`
       * this will install all the client-side JavaScript packages and you should see a `jspm_packages` folder now.
   6. In order to check sass style rules, we use scss-lint which requires ruby
      * Install ruby
      * After Ruby is installed, from a commandline: Execute `gem install scss_lint`
   7. You should be ready to go if you didn't receive any errors, look at the commands section on what you can do. Most common command for local development is `gulp serve`

#### Commands

on the commandline in the root of this repository execute:

`gulp <task>`

####Tasks

We are using a namespace schema so all tasks will be suffixed with .

- `build` - runs tests and on success builds into dist folder ( Copies HTML files, webpack bundles JS, Sass 
processes styles)
- `build-local` - same as the previous, but does not minify the code.
- `test` - runs lint and karma
- `watch` - runs watch and server
- `serve` - runs the dev server and opens the browser to the page
- `watch-test` - runs a watcher than reruns tests on file change
- `styles` - invokes sass to process .scss files and outputs to dist folder 

When the dev server is running, it will watch the code files and automatically reload the browser with the new code.  
When watching tests, gulp will watch code and tests and will execute the linter and tests and report to the console.  
Take note that you should pay attention to the command console for error reporting as well as the browser console for 
any errors that occur in the browser.

## Styles

Sass is being used as the preprocessor, refer to commands for executing the process step and the style guide for 
available style components using scss syntax.

Styles can be imported into a component via javascript using: (**NOTE** this feature is disabled until we have the need)

```import from 'componentStyle.scss'```

Importing a style in this way will cause the component to render a new style tag in the head of the document when 
it is loaded into the browser's memory. Be careful and make sure this is actually what you want to be doing.

Refer to the AEM Integration documentation on how integration of styles works.

### Shared Components

There is shared code respository located at: <https://stash.ltfinc.net/projects/SL/repos/shared-components/browse>.  
This code is where multiple teams can contribute to the global components and styles and minimize duplication of 
effort.  Currently, JSPM is being used to integrate the built code from the shared repository and integrated into the
 mylt code at build time as part of the gulp task pipeline.  There is an effort to get npm working with Nexus so that
  we can leverage npm to publish shared and third party integrated components.
  
 This will be used for common sass variables and any components constructed from here will be assumed to loaded 
 globally as defined. If new global components are needed, please refer to documentation on confluence: <https://confluence.ltfinc.net/display/SL/Shared+Libraries>

## Style guide

 The official living style guide can be found at: <https://ltfstyleguide.azurewebsites.net/?p=patterns-related-posts>.
 You can login using your e0000000@ltfinc.net account credentials.

 Developer guidelines as targeted at third party integration: <https://confluence.ltfinc.net/display/MYLT/3rd+Party+Style+Guidelines>

## AEM Integration

The process for integration with the AEM project is covered on confluence here: <https://confluence.ltfinc
.net/display/MYLT/AEM+3rd+Party+Code+Integration>
