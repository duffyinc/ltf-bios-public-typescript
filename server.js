// load express and create our app
var express = require('express');
var app = express();
var proxyMiddleware = require('http-proxy-middleware');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var https = require('https');

var knownOptions = {
	string: ['env', 'user'],
	default: { env: process.env.NODE_ENV || 'dev', user: 'Developer_member' }
};

var args = require('minimist')(process.argv.slice(2), knownOptions);
	var useQA = args.env ? args.env.toUpperCase() === 'QA' : false;
var username = args.user;

app.use(express.static('./dist'));

// log all requests to the console
app.use(morgan(':date[iso] :remote-addr :remote-user :method :url HTTP/:http-version :status :res[content-length] -' +
	' :response-time ms'));

// our api
var apiRouter = express.Router();

var loginUrl = 'https://frank-dev.lifetimefitness.com/api/mms/login.json';
if(useQA) {
	loginUrl = loginUrl.replace('dev', 'qa');
}

// get timesheet
apiRouter.get('/profile', function(req, res) {
	https.get(loginUrl + '?username='+username+'&password=Password1', function(resp){
		var profileData = '';
		resp.setEncoding('utf8');
		resp.on('data', function(data){
			profileData += data;
		});

		resp.on('end', function(){
			console.log(profileData);
			res.json(profileData);
		});
	}).on('error', function(err){
		res.json(err);
	});
});

// register api routes, prefixed with /api
app.use('/api', apiRouter);

// Example proxy setup for Program Registration API
app.use(proxyMiddleware('/api/registration', {
	target: 'https://qa-register.lifetimefitness.com',
	changeOrigin: true,
	headers: {
		// need to override the Origin header due to Chrome adding it to same-origin POST/PUT/DELETE requests
		// if we don't override it, the server will reject the requests due to them coming from http://localhost:3000
		// see http://stackoverflow.com/a/15514049/1785793 for more info
		'Origin' : 'http://localhost.lifetimefitness.com'
	}
}));

// app configuration
// use body parser so we can grab information from POST requests
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.listen(1337);
console.log('dev server listening on port 1337');
