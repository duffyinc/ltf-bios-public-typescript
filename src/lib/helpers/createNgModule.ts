///<reference path="../../../tools/typings/angularjs/angular.d.ts"/>
///<reference path="../../../tools/typings/bootstrap/bootstrap.d.ts"/>

import angular from 'angular';
import 'angular-route';
import 'ngprogress';
import 'angular-ui-router';
import 'angular-animate';

import MemberController from 'components/member/MemberController';
import SearchController from 'components/search/SearchController';
import DetailController from 'components/detail/DetailController';
import ConsultationController from 'components/consultation/ConsultationController';

import bioService from 'lib/services/bioService';
import bioSearchState from 'lib/factories/bioSearchState';
import bioTrainerInfo from 'lib/factories/bioTrainerInfo';

import multiSelect from 'lib/directives/multiSelect';
import multiSelectGroup from 'lib/directives/multiSelectGroup';
import Chiclets from 'lib/directives/chiclets';
import errSrc from 'lib/directives/errSrc';
import loadingIndicator from 'lib/directives/loaders';
import routeLoadingIndicator from 'lib/directives/loaders';

import 'angular-utils-pagination';

export default function(moduleName: string, MainController?: Function): ng.IModule {

	var module: ng.IModule = angular.module(moduleName, ['ngRoute', 'ui.router',
		'ngAnimate', 'ngProgress', 'angularUtils.directives.dirPagination'])
		//'ltt.programRegistration.api', 'ngAnimate','ng.deviceDetector','ngStorage'])
		.service('bioService', bioService)
		.factory('bioSearchState', bioSearchState)
		.factory('bioTrainerInfo', bioTrainerInfo)
		.directive('multiselect', multiSelect)
		.directive('multiselectGroup', multiSelectGroup)
		.directive('chiclets', Chiclets)
		.directive('errSrc', errSrc)
		.directive('loadingIndicator', loadingIndicator)
		.directive('routeLoadingIndicator', ['$rootScope', routeLoadingIndicator])
		.constant('bioRoutes', {
			search: {
				template: 'components/search/search.html',
				controller: 'SearchController',
				controllerAs: 'searchCtrl',
				route: '/'
			},
			detail: {
				template: 'components/detail/detail.html',
				controller: 'DetailController',
				controllerAs: 'detailCtrl',
				route: '/detail/:identifier'
			}
		})
		.config(['$locationProvider', '$routeProvider', '$httpProvider', 'bioRoutes', 'paginationTemplateProvider',
			function ($locationProvider, $routeProvider, $httpProvider, bioRoutes, paginationTemplateProvider) {

			$routeProvider
				.when(bioRoutes.search.route, {
					templateUrl: bioRoutes.search.template,
					controller: bioRoutes.search.controller,
					controllerAs: bioRoutes.search.controllerAs
				})
				.when(bioRoutes.detail.route, {
					templateUrl: bioRoutes.detail.template,
					controller: bioRoutes.detail.controller,
					controllerAs: bioRoutes.detail.controllerAs
				})
				.otherwise({ redirectTo: '/' });

				paginationTemplateProvider.setPath('components/pagination/pagination.html');
		}]
	);


	if (SearchController) {
		module.controller('SearchController', SearchController);
	}

	if (MainController) {
		module.controller('MemberController', MemberController);
	}

	if (DetailController) {
		module.controller('DetailController', DetailController);
	}

	if (ConsultationController) {
		module.controller('ConsultationController', ConsultationController);
	}

	module.run(['$rootScope', 'bioService', 'bioSearchState', ($rootScope: ng.IScope, bioService: bioService, bioSearchState: bioSearchState) => {

	}]);

	return module;
}
