export var BIOCONFIG = {
	DEBUGOPTIONS: {
		localJsonServices: false, // load mock services from local json files
		remoteServices: true // use absolute path for services
	},
	apiBasePath: '/api',
	apiRemoteBasePath: 'https://ltf-bio-dev.azurewebsites.net/api',
	apiPrefix: '/v1/trainers',
	html5Routes: false
};
