///<reference path="../../../tools/typings/angularjs/angular.d.ts"/>

export default function(): Chiclets {
	return {
		bindToController: true,
		controller: [
			'$scope',
			chicletsController
		],
		controllerAs: 'vm',
		restrict: 'E',
		scope: {
			filters: '=',
			onUpdateFilters: '&',
			homeLocation: '='
		},
		template: `<div class="container">
			<div class="row chiclets-container">
				<div class="col-xs-12">
					<div class="chiclet" ng-repeat="filter in vm.filters | orderBy: 'chicletOrder'">
						<span class="chiclet-label">{{ filter.Value }}{{ filter.Name }}</span>
						<button class="chiclet-close" ng-click="vm.removeChiclet(filter, filter.filterKey)"><span class="ico-close-sm"></span></button>
					</div>
				</div>
			</div>
		</div>`
	};

	function chicletsController($scope) {
		var vm = this;
		vm.updateChiclets = updateChiclets;
		vm.removeChiclet = removeChiclet;
		$scope.$watchCollection('vm.filters', updateChiclets);

		function updateChiclets(filters) {

		}

		function removeChiclet(chiclet, filterKey) {
			chiclet.IsSelected = false;
			var idx = _.indexOf(vm.filters, chiclet);
			if (idx !== -1) {
				vm.filters.splice(idx, 1);
			}
			vm.onUpdateFilters()(chiclet, filterKey);
		}
	}
}
