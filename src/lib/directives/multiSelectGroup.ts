///<reference path="../../../tools/typings/angularjs/angular.d.ts"/>

export default function(): multiSelectGroup {
	var isMultiSelectActive = false;

	// Close dropdown when clicking outside
	angular.element(document).mouseup(function (e) {
		if (angular.element('.multiselect').is(e.target))
			return;
		if (angular.element('.multiselect').has(e.target).length !== 0)
			return;
		closeMultiSelect();
	});

	// Close dropdown on 'esc' keypress
	angular.element(document).keyup(function (e) {
		if (e.which === 27)
			closeMultiSelect();
	});

	function closeMultiSelect() {
		if (!isMultiSelectActive)
			return;
		isMultiSelectActive = false;
		$(document).find('.multiselect-btn.is-active').removeClass('is-active').next('.multiselect-wrap').slideUp('fast');
		angular.element(document).triggerHandler('ltf:cmp:size', 'closing multiselect');
	}

	return {
		restrict: 'E',
		template: `<div class="multiselect">
			<button class="multiselect-btn" ng-click="toggleMenu(id)">{{ placeholder }}</button>
			<div class="multiselect-wrap">
				<div class="multiselect-group">
					<div ng-repeat="items in choices">
						<div class="multiselect-subheading">{{ items.Region }}</div>
						<div ng-repeat="item in items.Clubs">
							<input id="{{id}}-{{ item.Identifier }}" value="{{ item.Identifier }}" name="checkbox" type="checkbox" ng-model="item.IsSelected" ng-change="stateChanged(item, filterKey)">
							<label for="{{id}}-{{ item.Identifier }}">{{ item.Name }}</label>
						</div>
					</div>
				</div>
			</div>
		</div>`,
		scope: {
			choices: '=',
			filterKey: '=',
			onChange: '&',
			placeholder: '@placeholder'
		},
		link: function(scope, el, attrs) {
			scope.stateChanged = stateChanged;
			scope.id = attrs.id;

			$(el).find('.multiselect-btn').click(openMultiSelect);

			function openMultiSelect() {
				var msCurrent = $(this), msWrap = msCurrent.next('.multiselect-wrap'),
					// Detect the height of the wrap
					wrap = msCurrent.next(msWrap), msWrapHeight = wrap.height();
				isMultiSelectActive = true;
				if (msCurrent.hasClass('is-active'))
					return closeMultiSelect();
				if ($('.multiselect').not(msCurrent)) {
					$('.multiselect').each(function () {
						$('.multiselect-wrap').removeClass('is-active');
						$('.multiselect-btn').removeClass('is-active');
						$('.multiselect-wrap').hide();
					});
				}
				if (!msWrapHeight) {
					msWrapHeight = wrap.show().height();
					wrap.hide();
				}
				// Open current dropdown
				msCurrent.toggleClass('is-active');
				msCurrent.next(msWrap).slideToggle('fast');
				// Add scroll indicator when list is taller than the max-height of the .multiselect-wrap
				// Disabled per Mike Litecky
				//if (msList.height() > msWrapHeight) {
				//	msWrap.addClass('is-tall');
				//} else {
				//	msWrap.removeClass('is-tall'); // TODO: This check should be re-run whenever the list is updated (remove the scroll indicator when enough items are removed to make scrolling unnecessary)
				//}
				// Event size change
				angular.element(document).triggerHandler('ltf:cmp:size', 'opening multiselect');
			}

			function stateChanged(item, filterKey) {

				if (scope.filterItems) {
					var filterItem = _.find(scope.filterItems, function (filterItem) {
						return filterItem.name === item.name;
					});
					filterItem.selected = item.selected;
					filterItem.chicletOrder = 0;
				} else {
					item.chicletOrder = 0;
				}

				scope.onChange()(item, filterKey);
			}
		}
	}
}
