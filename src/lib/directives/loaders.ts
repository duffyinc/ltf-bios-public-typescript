///<reference path="../../../tools/typings/angularjs/angular.d.ts"/>

export default function($rootScope): routeLoadingIndicator {
	return {
		restrict: 'E',
		template: "<div ng-show='isRouteLoading' class='container loading-container'><div class='loading-spinner-lg'></div></div>",
		link: function(scope, elem, attrs) {
			scope.isRouteLoading = false;

			$rootScope.$on('$routeChangeStart', function() {
				//console.log('route change starting');
				scope.isRouteLoading = true;
			});

			$rootScope.$on('$routeChangeSuccess', function() {
				//console.log('route change ending');
				scope.isRouteLoading = false;
			});
		}
	}
}

export default function(): loadingIndicator {
	return {
		restrict: 'E',
		compile: function() {
			return {
				pre: function(scope, elem, attrs) {
					scope.condition = attrs.condition || 'isLoading';
				}
			}
		},
		template: "<div ng-show='{{ condition }}' class='loading-container'><div class='loading-spinner-lg'></div></div>",
	}
}
