///<reference path="../../../tools/typings/angularjs/angular.d.ts"/>

export var BIOCONFIG = {
	DEBUGOPTIONS: {
		localJsonServices: false, // load mock services from local json files
		remoteServices: true // use absolute path for services
	},
	apiBasePath: '/api',
	apiRemoteBasePath: 'https://ltf-bio-dev.azurewebsites.net/api',
	apiPrefix: '/v1/trainers',
	html5Routes: false
};

export default class BioService {

	public static $inject: string[] = ['$q', '$http'];

	private config: String = {};
	private $q: ng.IQService;
	private $http: ng.IHttpService;

	constructor($q: ng.IQService, $http: ng.IHttpService) {
		this.$q = $q;
		this.$http = $http;
	}

	public returnApiPath = function (part, options) {
		var path = BIOCONFIG.DEBUGOPTIONS.remoteServices ? BIOCONFIG.apiRemoteBasePath : BIOCONFIG.apiBasePath;
		path += BIOCONFIG.apiPrefix + part + (BIOCONFIG.DEBUGOPTIONS.localJsonServices ? '.json' : '');

		if (options) {
			if (!BIOCONFIG.DEBUGOPTIONS.localJsonServices) {
				if (options.append) {
					path += '/' + options.append;
				}
			}
		}

		return path;
	}

	public get(path, callback, params) {
		var callConfig = $.extend({}, this.config, params);

		return this.$http.get(path, callConfig).then(function (response) {
			callback(true, response.data);
		}, function (response) {
			callback(false, response);
		});
	}

	/*public getMultiple = function (paths, callback) {
		var results = [];
		ng.forEach(paths, function (path) {
			results.push($http.get(path, config));
		});

		return this.$q.all(results).then(function (response) {
			this.returned = response;
			callback(true, this.returned);
		}, function (response) {
			callback(false, response);
		});
	} */

	public put(path, content, callback) {
		return this.$http.put(path, content, this.config).then(function (response) {
			var returned = response.data;
			callback(true, returned);
		}, function (response) {
			callback(false, response);
		});
	}

	/*public putMultiple(pathsAndContent, callback) {
		var results = [];
		angular.forEach(pathsAndContent, function (item) {
			results.push($http.put(item.path, item.content, config));
		});

		return $q.all(results).then(function (response) {
			//console.log('all puts completed');
			this.returned = response;
			callback(true, this.returned);
		}, function (response) {
			callback(false, response);
		});
	}*/

  public getMessage():String {
    return 'Hello from the bioService!';
  }

}
