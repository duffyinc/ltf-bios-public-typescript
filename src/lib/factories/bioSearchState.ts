///<reference path="../../../tools/typings/angularjs/angular.d.ts"/>

export default function(): BioSearchState {

	var data = {};

	data.totalResultsCount = 0;
	data.shownResultsCount = 0;

	// all available filters
	data.filters = {};
	data.filters.Club = [];
	data.filters.Times = [];
	data.filters.AreasOfFocus = [];
	data.filters.TrainingStyle = [];
	data.filters.Services = [];

	// currently active filters
	data.activeFilters = {};
	data.activeFilters.Club = [];
	data.activeFilters.Times = [];
	data.activeFilters.AreasOfFocus = [];
	data.activeFilters.TrainingStyle = [];
	data.activeFilters.Services = [];

	// flattened array for visible filters
	data.visibleFilters = [];

	// full results
	data.results = [];

	// filtered results
	data.filteredResults = [];

	// sort method
	data.sortMethod = '';

	// pagination
	data.pageSize = 18;
	data.currentPage = 1;

	data.activeLocationsList = function () {
		var locations = '',
				idx = 0;

		angular.forEach(data.activeFilters.Club, function (location) {
			if (idx > 0) {
				locations += ',';
			}
			locations += location.Identifier;
			idx++;
		});
		return locations;
	}

	data.translateToVisibleFilters = function () {
		data.visibleFilters = [];
		data.translateSingleFilter(data.activeFilters.Club, 'Club');
		data.translateSingleFilter(data.activeFilters.Times, 'Times');
		data.translateSingleFilter(data.activeFilters.AreasOfFocus, 'AreasOfFocus');
		data.translateSingleFilter(data.activeFilters.TrainingStyle, 'TrainingStyle');
	};

	data.translateSingleFilter = function(filters, filterKey) {
		angular.forEach(filters, function (item) {
			var tItem = item;
			tItem.filterKey = filterKey;
			data.visibleFilters.push(tItem);
		});
	};

	return data;
}
