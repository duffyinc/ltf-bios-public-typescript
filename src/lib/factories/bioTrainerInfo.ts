///<reference path="../../../tools/typings/angularjs/angular.d.ts"/>

export default function(): BioTrainerInfo {
	var data = {};

	data.trainerIdentifier = null;
	data.trainer = null;

	data.lowestRate = function (result) {
		var rate = null;
		for (var i = 0; i < result.Services.length; i++) {
			var tempRate = result.Services[i].Rate;
			if (rate === null) {
				rate = tempRate;
			} else if (tempRate < rate) {
				rate = tempRate;
			}
		}
		return rate;
	};

	return data;
}
