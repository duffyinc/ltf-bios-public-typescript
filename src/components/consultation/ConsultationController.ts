import bioService from 'lib/services/bioService';
import bioTrainerInfo from 'lib/factories/bioTrainerInfo';
import BIOCONFIG from 'lib/helpers/bioAppConfig';

export default class ConsultationController {
	public static $inject: string[] = ['$window', '$location', '$rootScope', '$scope',
	'$routeParams', 'bioService', 'bioTrainerInfo', 'ngProgressFactory'];

	protected bioService: bioService;
	protected bioTrainerInfo: bioTrainerInfo;
	protected $rootScope: ng.IRootScopeService;
  protected $window: ng.IWindowService;
  protected $location: ng.ILocationService;
	protected $routeParams: ng.IRouteParamsService;
	protected $scope : ng.IScope;
	protected ngProgressFactory;

	constructor($window: ng.IWindowService, $location: ng.ILocationService, $rootScope: ng.IRootScopeService, $scope: ng.IScope, $routeParams: ng.IRouteParamsService, bioService: bioService, bioTrainerInfo: bioTrainerInfo, ngProgressFactory) {
		this.$window = $window;
		this.$location = $location;
		this.$rootScope = $rootScope;
		this.$scope = $scope;
		this.$routeParams = $routeParams;
		this.bioService = bioService;
		this.bioTrainerInfo = bioTrainerInfo;
		this.ngProgressFactory = ngProgressFactory;

		this.init();
	}

	private init() {
		let self = this;
		let safari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);

		this.$scope.progressbar = this.ngProgressFactory.createInstance();
		this.$scope.progressbar.setParent($('.consultation-progress>.bar')[0]);

		this.$scope.questions = [];
		this.$scope.currentQuestionNumber = 1;
		this.$scope.totalQuestionNumber = 0;
		this.$scope.currentQuestionIndex = 0;
		this.$scope.percentIncrement = 0;
		this.$scope.answers = [];
		this.$scope.selectedAnswerTotal = 0;
		this.$scope.maxAllowedAnswers = 1;
		this.$scope.isComplete = false;
		this.$scope.form = {};
		this.$scope.locations = [];
		this.$scope.summary = {};
		this.$scope.form.selectedClubText = "Select Location";
		this.$scope.isSafari = safari;

		this.$scope.$on('consultationContinue',  function(){
			$('#unsavedChangesModal').modal('hide');
		});

		this.$scope.$on('consultationExit', function () {
			self.reset();
			$('#unsavedChangesModal').modal('hide');
			$("#consultationModal").toggleClass('active');
		});

		this.$scope.$on('consultationOpen', (event: ng.IAngularEvent, ?args: Object) => {
			$("html, body").animate({ scrollTop: 0 }, "slow");
			self.reset();
			self.calc();

			if(args) self.setSelectedClub(args);
			$("#consultationModal").toggleClass('active');
		});

		this.retrieveQuestions();
		this.retrieveClubs();
	}

	private setProgress = () => {
		let $scope = this.$scope;

		$scope.progressbar.set($scope.percentIncrement * $scope.currentQuestionNumber);
	};

	private setMaxAnswers = () => {
		let $scope = this.$scope;

		if($scope.questions[$scope.currentQuestionIndex] !== undefined){
			if ($scope.questions[$scope.currentQuestionIndex].AllowMultipleAnswers) {
				$scope.maxAllowedAnswers = $scope.questions[$scope.currentQuestionIndex].Options.length;
			}
			else {
				$scope.maxAllowedAnswers = 1;
			}
		}
	};

	private setSelectedAnswers = () => {
		let self = this;
		let $scope = self.$scope;

		$scope.selectedAnswerTotal = 0;
		if($scope.questions[$scope.currentQuestionIndex] !== undefined){
			$scope.questions[$scope.currentQuestionIndex].Options.forEach(function(item, index){
				if(self.IsSelected(item.Id)){
					$scope.selectedAnswerTotal += 1;
				}
			});
		}
	};

	private clearMessage = () => {
		let $scope = this.$scope;

		$scope.form = {
			PreferredTimes: [
				{
					Key: 1,
					Value: 'Morning',
					IsSelected: false
				},
				{
					Key: 2,
					Value: 'Afternoon',
					IsSelected: false
				},
				{
					Key: 3,
					Value: 'Evening',
					IsSelected: false
				}
			]
		};
		if ($scope.consultaionForm) {
			$scope.consultaionForm.$setPristine();
		}
	};

	private reset = () => {
		let $scope = this.$scope;

		$scope.currentQuestionNumber = 1;
		$scope.totalQuestionNumber = 0;
		$scope.currentQuestionIndex = 0;
		$scope.percentIncrement = 0;
		$scope.answers = [];
		$scope.selectedAnswerTotal = 0;
		$scope.maxAllowedAnswers = 1;
		$scope.isComplete = false;
		this.clearMessage();

		var clubLabel = $('.label.consultation-clubs');
		var clubInput = $('multiselect#consultation-clubs button');
		clubLabel.removeClass('invalid-icon');
		clubInput.removeClass('invalid-border');

		$scope.form.selectedClubText = "Select Location";
	};

	private calc = () => {
		let $scope = this.$scope;

		$scope.totalQuestionNumber = $scope.questions.length + 1;
		$scope.percentIncrement = (100 / $scope.totalQuestionNumber);

		this.setMaxAnswers();
		this.setProgress();
	};

	private retrieveQuestions = () => {
		let self = this;
		let $scope = this.$scope;

		$scope.isLoadingQuestions = true;
		var data = this.bioService.returnApiPath('/consultation');

		this.bioService.get(data, function (success, response) {
			$scope.isLoadingQuestions = false;

			if (success) {
				self.reset();
				$scope.questions = response;
				self.calc();
				console.log(response);
			}
		});

	};

	private retrieveClubs = () => {
		var data = this.bioService.returnApiPath('/filters');
		let self = this;
		this.bioService.get(data, function (success, response) {

			if (success) {
				self.$scope.locations = response.Locations;
				console.log(response);
			}
		});
	};

	private setSelectedClub = (clubIdentifier) => {
		let $scope = this.$scope;
		$scope.form.ClubIdentifier = clubIdentifier;

		var abort = false;

		for (var iLocation = 0, lenLocation = $scope.locations.length; iLocation < lenLocation && !abort; iLocation++) {
			for (var iClub = 0, lenClub = $scope.locations[iLocation].Clubs.length; iClub < lenClub && !abort; iClub++) {
				if($scope.locations[iLocation].Clubs[iClub].Identifier === clubIdentifier){
					$scope.locations[iLocation].Clubs[iClub].IsSelected = true;
					$scope.form.selectedClubText = $scope.locations[iLocation].Clubs[iClub].Name;
					abort = true;
				}
			}
		}
	}

	private validateClubSelection = () => {
		let $scope = this.$scope;
		var isValid = false;

		var clubLabel = $('.label.consultation-clubs');
		var clubInput = $('multiselect#consultation-clubs button');

		if($scope.form.ClubIdentifier !== undefined && $scope.form.ClubIdentifier !== ''){
			isValid = true;

			if(clubLabel.hasClass('invalid-icon')){
				clubLabel.removeClass('invalid-icon');
			}

			if(clubInput.hasClass('invalid-border')){
				clubInput.removeClass('invalid-border');
			}
		} else {
			if(!clubLabel.hasClass('invalid-icon')){
				clubLabel.addClass('invalid-icon');
			}

			if(!clubInput.hasClass('invalid-border')){
				clubInput.addClass('invalid-border');
			}
		}

		return isValid;
	};


	private IsSelected = (id) => {
		return this.$scope.answers.indexOf(id) !== -1;
	};

	private IsValidOption = (id) => {
		let $scope = this.$scope;
		var isMaxSelected = $scope.selectedAnswerTotal === $scope.maxAllowedAnswers;

		return !(isMaxSelected && !this.IsSelected(id));
	};

	private ChangeAnswer = (id) => {
		let $scope = this.$scope;
		var valid = this.IsValidOption(id)

		if (this.IsValidOption(id)) {
			if ($scope.answers.indexOf(id) !== -1) {
				var i = $scope.answers.indexOf(id);
				$scope.answers.splice(i, 1);
				$scope.selectedAnswerTotal -= 1;
			} else {
				$scope.answers.push(id);
				$scope.selectedAnswerTotal += 1;
			}
		} else {
			var i = $scope.currentQuestionIndex;

			$scope.answers.splice(i, 1);
			$scope.answers.push(id);
		}
	};

	private NextQuestion = () => {
		this.$scope.currentQuestionIndex += 1;
		this.$scope.currentQuestionNumber += 1;
		this.setSelectedAnswers();
		this.setMaxAnswers();
		this.setProgress();
	};

	private PreviousQuestion = () => {
		console.log(this.$scope);
		if (this.$scope.currentQuestionNumber > 1) {
			this.$scope.currentQuestionIndex -= 1;
			this.$scope.currentQuestionNumber -= 1;
			this.setSelectedAnswers();
			this.setMaxAnswers();
			this.setProgress();
		}
	};

	private IsAnswered = () => {
		let $scope = this.$scope;
		return $scope.selectedAnswerTotal > 0 && $scope.selectedAnswerTotal <= $scope.maxAllowedAnswers;
	};

	private OnQuestions = () => {
		return !this.OnThankYou() && !this.OnContactForm();
	};

	private OnContactForm = () => {
		let $scope = this.$scope;
		return $scope.currentQuestionNumber === $scope.totalQuestionNumber && !$scope.isComplete;
	};

	private OnThankYou = () => {
		return this.$scope.isComplete;
	};


	private SelectClub = (item, filterKey) => {
		let $scope = this.$scope;
		if (item.IsSelected) {

			for (var iLocation = 0, lenLocation = $scope.locations.length; iLocation < lenLocation; iLocation++) {
				for (var iClub = 0, lenClub = $scope.locations[iLocation].Clubs.length; iClub < lenClub; iClub++) {
					if($scope.locations[iLocation].Clubs[iClub].Identifier !== item.Identifier){
						$scope.locations[iLocation].Clubs[iClub].IsSelected = false;
					}
				}
			}

			$scope.form.ClubIdentifier = item.Identifier;
			$scope.form.selectedClubText = item.Name;
		} else {
			$scope.form.ClubIdentifier = undefined;
			$scope.form.selectedClubText = "Select Location";
		}

		this.validateClubSelection();

	};

	private ConfirmExit = () => {
		$('#unsavedChangesModal').modal();
	};

	private Finish = () => {
		let self = this;
		let $scope = self.$scope;

		//if these are done as a condition && together then one potentially won't execute
		var formIsValid = $scope.consultaionForm.$valid;
		var clubIsValid = this.validateClubSelection();

			if (formIsValid && clubIsValid) {

				var consultationRequest = {
					Contact: $scope.form,
					Answers: $scope.answers
				};

				var data = this.bioService.returnApiPath('/consultation');

				this.bioService.put(data, consultationRequest, function (success, response) {
					$scope.summary.FirstName = $scope.form.SubmitterFirstName;
					self.clearMessage();
					$scope.isComplete = true;
				});

			} else {
				$scope.consultaionForm.consultationFirstName.$setDirty();
				$scope.consultaionForm.consultationLastName.$setDirty();
				$scope.consultaionForm.consultationPhone.$setDirty();
				$scope.consultaionForm.consultationEmail.$setDirty();
			}
	};

	private Close = () => {
		$("#consultationModal").toggleClass('active');
	};
}
