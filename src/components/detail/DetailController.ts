import bioService from 'lib/services/bioService';
import bioTrainerInfo from 'lib/factories/bioTrainerInfo';
import BIOCONFIG from 'lib/helpers/bioAppConfig';

export default class DetailController {
	public static $inject: string[] = ['$window', '$location', '$scope', '$rootScope', '$routeParams', 'bioService', 'bioRoutes', 'bioTrainerInfo'];

	protected bioService: bioService;
	protected bioTrainerInfo: bioTrainerInfo;
	protected bioRoutes: bioRoutes;
	protected $window: ng.IWindowService;
	protected $location: ng.ILocationService;
	protected $routeParams: ng.IRouteParamsService;
	protected $scope: ng.IScope;
	protected $rootScope: ng.IRootScope;

	constructor($window: ng.IWindowService, $location: ng.ILocationService, $scope: ng.IScope, $rootScope: ng.IRootScope, $routeParams: ng.IRouteParamsService, bioService: bioService, bioRoutes: bioRoutes) {
		this.$window = $window;
		this.$location = $location;
		this.$scope = $scope;
		this.$rootScope = $rootScope;
		this.$routeParams = $routeParams;
		this.bioService = bioService;
		this.bioTrainerInfo = bioTrainerInfo();
		this.bioRoutes = bioRoutes;

		this.isSearching();
		this.init();
	}

	private init() {
		this.$scope.trainer = null;
		this.$scope.testimonialPageSize = 2;
		this.$scope.visibleTestimonials = 0;
		this.$scope.isLoading = false;
		this.$scope.bioTrainerInfo = this.bioTrainerInfo;
		this.$scope.newTestimonial = {};
		this.$scope.validTrainer = true;
		this.$rootScope.isLoading = false;

		this.retrieve();
	}

	private retrieve = () => {
		let $scope = this.$scope;
		let self = this;
		let data = this.bioService.returnApiPath('/profile', { append: this.$routeParams.identifier });

		$scope.isLoading = true;
		$scope.visibleTestimonials = 0;
		this.bioTrainerInfo.trainer = null;
		this.bioTrainerInfo.trainerIdentifier = $routeParams.identifier;

		this.bioService.get(data, function (success, response) {
			$scope.isLoading = false;

			if (success) {
				self.bioTrainerInfo.trainer = response;

				self.bioTrainerInfo.trainer.LowestRate = self.bioTrainerInfo.lowestRate(self.bioTrainerInfo.trainer);

				self.showMoreTestimonials(true);
			} else {
				if (response.status == 301) {
					// trainer has been terminated
					$window.location = self.bioRoutes.search.route;
				} else if (response.status == 404) {
					// no trainer found
					$scope.validTrainer = false;
				}
			}
		});

	};

	private displayDayRange = (day) => {
		var dayDescription = day.FromDay;
		if (day.ToDay !== null) {
			dayDescription += '-' + day.ToDay;
		}
		dayDescription += ': ';
		var timeCount = 0;
		angular.forEach(day.Times, function (time) {
			if (timeCount > 0) {
				dayDescription += ', ';
			}
			dayDescription += time.Label;
			timeCount++;
		});
		return dayDescription;
	};

	private timesList = (day) => {
		var times = day.Times,
			timesValue = '',
			count = 0;

		angular.forEach(times, function (time) {
			if (count > 0) {
				timesValue = timesValue + ', ';
			}
			timesValue = timesValue + time.Label;
			count++;
		});
		return timesValue;
	};

	private areaOfFocusImage = (item, banner) => {
		var key = 'AreaOfFocus_Non_Primary'
		if (banner) {
			key = 'AreaOfFocus_Primary';
		}
		var image = _.find(item.Photos, { 'PhotoTypeName': key });
		return image ? image.Path : '';
	};

	private testimonialModal = () => {
		$('#testimonialModal').modal();
	};

	private sendTestimonial = () => {
		let self = this;
		let $scope = this.$scope;

		if ($scope.testimonialForm.$valid) {
			let data = this.bioService.returnApiPath('/profile', { append: this.$routeParams.identifier + '/testimonial' });

			this.bioService.put(data, $scope.newTestimonial, function (success, response) {
				self.clearTestimonial();
			});

			$('#testimonialModal').modal('hide');
		} else {
			$scope.testimonialForm.testimonialFirstName.$setDirty();
			$scope.testimonialForm.testimonialLastName.$setDirty();
			$scope.testimonialForm.testimonialBody.$setDirty();
			$scope.testimonialForm.testimonialMemberNumber.$setDirty();
		}
	};

	private clearTestimonial = () => {
		this.$scope.newTestimonial = {};
		if (this.$scope.testimonialForm) {
			this.$scope.testimonialForm.$setPristine();
		}
	};

	private showMoreTestimonials = (initial) => {
		//var toShow = Math.min($scope.trainerInfo.trainer.Testimonials.length, $scope.visibleTestimonials + $scope.testimonialPageSize);
		var toShow = initial ? this.$scope.testimonialPageSize : this.$scope.trainerInfo.trainer.Testimonials.length;

		this.$scope.visibleTestimonials = toShow;
	};

	private consultationOpen = (args) => {
		this.$rootScope.$broadcast('consultationOpen', args);
	}

	private isSearching = () => {
		this.$rootScope.$broadcast('isSearching', false);
	}

	private messageOpen = (event: ng.IAngularEvent, trainer) => {
		event.preventDefault();
		event.stopPropagation();

    this.$rootScope.$broadcast('messageOpen', trainer);
	};
}
