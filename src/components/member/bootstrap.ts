import angular from 'angular';
import { moduleName } from './MemberModule';

var el = $('#lt-member')[0];

$(() => {
	angular.bootstrap(el, [moduleName]);
});
