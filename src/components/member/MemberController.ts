/// <reference path="../../../tools/typings/AEM/LtFramework.d.ts" />
///<reference path="../../../tools/typings/ngjs/ng.d.ts"/>
///<reference path="../../../tools/typings/ng-ui-router/ng-ui-router.d.ts"/>

import bioService from 'lib/services/bioService';
import bioSearchState from 'lib/factories/bioSearchState';
import bioTrainerInfo from 'lib/factories/bioTrainerInfo';
import BIOCONFIG from 'lib/helpers/bioAppConfig';

export default class MemberController {
	public static $inject: string[] = ['$window', '$location', '$rootScope', '$scope', '$routeParams', '$timeout', 'bioService', 'bioSearchState', 'bioTrainerInfo'];

	protected loadFormContent: boolean = false;
	protected bioService: bioService;
	protected bioSearchState: bioSearchState;
	protected bioTrainerInfo: bioTrainerInfo;
	protected $window: ng.IWindowService;
	protected $location: ng.ILocationService;
	protected $scope: ng.IScope;
	protected $timeout: ng.ITimeout;

	constructor($window: ng.IWindowService, $location: ng.ILocationService, $rootScope: ng.IRootScopeServicem, $scope: ng.IScope,
		$routeParams: ng.IRouteParams, $timeout: ng.ITimeout, bioService: bioService) {
		this.$window = $window;
		this.$location = $location;
		this.$scope = $scope;
		this.$rootScope = $rootScope;
		this.$routeParams = $routeParams;
		this.$timeout = $timeout;
		this.bioService = bioService;
		this.bioSearchState = bioSearchState();
		this.bioTrainerInfo = bioTrainerInfo();
		this.init();
	}

	private init() {
		let self = this;

		this.$scope.newMessage = {};
		this.$scope.trainer = {};
		this.$scope.showThankYou = false;
		this.$scope.timer;
		this.$scope.sendingMessage = false;

		this.$scope.$on('isSearching', function (event, data) {
		  self.$rootScope.isSearching = data;
		});

		this.$scope.$on('messageOpen', (event: ng.IAngularEvent, ?args: Object) => {
      $("html, body").animate({ scrollTop: 0 }, "slow");
      self.messageModal(args);
    });

    this.clearMessage();
	}

	private messageModal = (trainer) => {
		if (typeof trainer != 'undefined') {
			this.bioTrainerInfo.trainer = trainer;
		}

		$('#messageModal').modal();
	}

	private sendMessage = () => {
		let self = this;
		let $scope = this.$scope;

		if ($scope.messageForm.$valid) {
			this.$scope.sendingMessage = true;
			var data = this.bioService.returnApiPath('/profile', { append: self.bioTrainerInfo.trainer.ProfileIdentifier + '/contact' });

			this.bioService.put(data, this.$scope.newMessage, function (success, response) {
				self.$scope.sendingMessage = false;
				self.$scope.showThankYou = true;

				self.$timeout.cancel(self.$scope.timer);

				self.$scope.timer = self.$timeout(function() {
					$('#messageModal').modal('hide');
					self.clearMessage();
				}, 4000);
			});
		} else {
			$scope.messageForm.messageFirstName.$setDirty();
			$scope.messageForm.messageLastName.$setDirty();
			$scope.messageForm.messageBody.$setDirty();
			$scope.messageForm.messagePhone.$setDirty();
			$scope.messageForm.messageEmail.$setDirty();
		}
	}

	private clearMessage = () => {
		this.$scope.newMessage = {
			PreferredTimes: [
				{
					Key: 1,
					Value: 'Morning',
					IsSelected: false
				},
				{
					Key: 2,
					Value: 'Afternoon',
					IsSelected: false
				},
				{
					Key: 3,
					Value: 'Evening',
					IsSelected: false
				}
			]
		};

		if (this.$scope.messageForm) {
			this.$scope.messageForm.$setPristine();
		}

		this.$scope.sendingMessage = false;
		this.$scope.showThankYou = false;
	}

	private consultationExit = () => {
		this.$rootScope.$broadcast('consultationExit');
	}

	private consultationContinue = () => {
		this.$rootScope.$broadcast('consultationContinue');
	}
}
