///<reference path="../../../tools/typings/angularjs/angular.d.ts"/>

import createNgModule from 'lib/helpers/createNgModule';
import Controller from './MemberController';

var moduleName: string = 'lt.Member';

var module: ng.IModule = createNgModule(moduleName, Controller);

export { moduleName as moduleName };
export default module;
