/// <reference path="../../../tools/typings/AEM/LtFramework.d.ts" />
///<reference path="../../../tools/typings/ngjs/angular.d.ts"/>

import _ from 'vendor/lodash';
import 'angular-utils-pagination';
import bioService from 'lib/services/bioService';
import bioSearchState from 'lib/factories/bioSearchState';
import BIOCONFIG from 'lib/helpers/bioAppConfig';

export default class SearchController {
	public static $inject: string[] = ['$window', '$location', '$rootScope', '$scope', 'bioService', 'bioSearchState'];

	protected bioService: bioService;
	protected bioSearchState: bioSearchState;
  	protected $window: angular.IWindowService;
  	protected $location: angular.ILocationService;
	protected $rootScope: ng.IRootScopeService;
	protected $scope: ng.IScope;

	constructor($window: angular.IWindowService, $location: angular.ILocationService, $rootScope: ng.IRootScopeService, $scope: angular.IScope, bioService: bioService, bioSearchState: bioSearchState) {
		this.$window = $window;
		this.$location = $location;
		this.$scope = $scope;
		this.bioService = bioService;
		this.bioSearchState = bioSearchState;
		this.$rootScope = $rootScope;
		this.init();
	}

	private init = () => {
		let self = this;
		this.$rootScope.filtersOpen = false;
		this.$scope.isLoading = false;
		this.$scope.isLoadingFilters = false;
		this.$scope.search = this.bioSearchState;
		this.$scope.newMessage = {};
		this.$scope.helpMessage = 'This is the default help text';

		this.$scope.sortOptions = [
			{
				Value: 'Name: A-Z',
				Key: 'az',
				SortValue: ['FirstName', 'LastNameInitial']
			},
			{
				Value: 'Name: Z-A',
				Key: 'za',
				SortValue: ['-FirstName', '-LastNameInitial']
			},
			{
				Value: 'Price: Low - High',
				Key: 'lowhigh',
				SortValue: 'LowestRate'
			},
			{
				Value: 'Price: High - Low',
				Key: 'highlow',
				SortValue: '-LowestRate'
			}
		];

		this.$scope.$watchGroup(['search.filteredResults', 'search.currentPage'], function () {
			self.updateResultCounts();
		});

		this.isSearching();
		this.retrieveFilters();
	}

	private writeRoute = (route: String) => {
		return (BIOCONFIG.html5Routes ? '' : '/#') + route;
	};

	private retrieveFilters = () => {
		let self = this;
		let $scope = self.$scope;
		let data = this.bioService.returnApiPath('/filters');

		this.bioService.get(data, function (success, response) {
			$scope.isLoadingFilters = false;

			if (success) {
				$scope.search.filters.Club = response.Locations;
				$scope.search.filters.Times = response.Times;
				$scope.search.filters.AreasOfFocus = response.AreasOfFocus;
				$scope.search.filters.TrainingStyle = response.TrainingStyles;
				$scope.search.filters.Services = response.Services;

				self.validateCurrentFilters(true); // initial search when user first comes to page
			}
		});
	}

	private findMaxChicletOrder = () => {
		if (this.$scope.search.visibleFilters.length > 0) {
			return _.maxBy(this.$scope.search.visibleFilters, function (i) {
				return i.chicletOrder;
			}).chicletOrder;
		} else {
			return 0;
		}
	};

	private getFiltersFromQueryString() {
		var params = this.$location.search();
		var scope = this.$scope;

		// add clubs
		if (params.l) {
			angular.forEach(params.l.split(','), function (item) {
				angular.forEach($scope.search.filters.Club, function (filterGroup) {
					angular.forEach(filterGroup.Clubs, function (filterItem) {
						if (filterItem.Identifier.toLowerCase() == item.toLowerCase()) {
							filterItem.IsSelected = true;
							this.updateSelectedFilters(filterItem, 'Club');
						}
					});
				});
			});
		}

		// add areas of focus
		if (params.aof) {
			angular.forEach(params.aof.split(','), function (item) {
				angular.forEach($scope.search.filters.AreasOfFocus, function (filterItem) {
					if (filterItem.Key.toLowerCase() == item.toLowerCase()) {
						filterItem.IsSelected = true;
						this.updateSelectedFilters(filterItem, 'AreasOfFocus');
					}
				});
			});
		}

		// add schedule
		if (params.tod) {
			angular.forEach(params.tod.split(','), function (item) {
				angular.forEach($scope.search.filters.Times, function (filterItem) {
					if (filterItem.Value.toLowerCase() == item.toLowerCase()) {
						filterItem.IsSelected = true;
						this.updateSelectedFilters(filterItem, 'Times');
					}
				});
			});
		}

		// add coaching/training style
		if (params.cs) {
			angular.forEach(params.cs.split(','), function (item) {
				angular.forEach($scope.search.filters.TrainingStyle, function (filterItem) {
					if (filterItem.Key.toLowerCase() == item.toLowerCase()) {
						filterItem.IsSelected = true;
						this.updateSelectedFilters(filterItem, 'TrainingStyle');
					}
				});
			});
		}

		// add service offering
		if (params.so) {
			var so = params.so.split(',')[0];
			var soObj;
			angular.forEach($scope.search.filters.Services, function (filterItem) {
				if (filterItem.Key.toLowerCase() == so.toLowerCase()) {
					this.filterByService(filterItem);
				}
			});
		}
	};

	private updateSelectedFilters = (item, filterKey) => {
		let $scope = this.$scope;

		if ($scope.search.activeFilters[filterKey]) {
			if (item.IsSelected) {
				item.chicletOrder = this.findMaxChicletOrder() + 1;
				$scope.search.activeFilters[filterKey].push(item);
				//console.log('added filter for ' + filterKey + ':');
				//console.log(item);
			} else {
				var idx = $scope.search.activeFilters[filterKey].indexOf(item);
				var temp = $scope.search.activeFilters[filterKey].splice(idx, 1);
				//console.log('removed filter for ' + filterKey + ':');
				//console.log(temp);
			}
		}

		this.$scope.search.translateToVisibleFilters();

		if (filterKey == 'Club') {
			this.validateCurrentFilters();
		} else {
			this.filterSearchResults();
		}
	}

	private updateSelectedSort = (item) => {
		let $scope = this.$scope;

		angular.forEach($scope.sortOptions, function (option) {
			if (option == item) {
				option.IsSelected = true;
			} else {
				option.IsSelected = false;
			}
		});

		$scope.search.sortMethod = item;
	};

	private toggleFilters = ($event) => {
		this.$rootScope.filtersOpen = !this.$rootScope.filtersOpen;
	};


	private validateCurrentFilters = (initial) => {
		if (initial) {
			// TODO: pull any query string values from URL and select proper filters (then remove query params from URL?)
			this.getFiltersFromQueryString();
		}

		//console.log($scope.search.activeFilters);

		if (this.$scope.search.activeFilters.Club.length > 0) {
			this.executeSearch();
		} else {
			this.$scope.search.results = [];
			this.$scope.search.filteredResults = [];
			// TODO: if no search selected, what happens? modal w/ zip code box?
		}
	};

	private executeSearch = () => {
		let self = this;
		let $scope = this.$scope;
		let data = this.bioService.returnApiPath('/search');

		$scope.isLoading = true;

		this.bioService.get(data, function (success, response) {
			//console.log(response);
			$scope.isLoading = false;

			if (success) {
				$scope.search.results = response;

				self.filterSearchResults();
			}
		}, { params: { 'club': $scope.search.activeLocationsList() } });
	};

	private filterByService = (service) => {
		if (typeof service == 'undefined') {
			this.$scope.search.activeFilters.Services = [];
		} else {
			this.$scope.search.activeFilters.Services = [service];
		}

		this.filterSearchResults();
	};

	private selectRate = (result) => {
		let self = this;

		if (self.$scope.search.activeFilters.Services.length > 0) {
			for (var i = 0; i < result.Services.length; i++) {
				if (result.Services[i].Key == self.$scope.search.activeFilters.Services[0].Key) {
					result.currentRate = result.Services[i].Rate;
					return;
				}
			}
		}
		result.currentRate = result.LowestRate;
	};

	private filterSearchResults = () => {
		// empty matching array
		this.$scope.search.filteredResults = [];
		let self = this;
		// loop through total results array
		angular.forEach($scope.search.results, function (result) {
			// for each result, check if each filter matches
			if (self.filterBySingleFacet(result, 'AreasOfFocus') && self.filterBySingleFacet(result, 'Times') && self.filterBySingleFacet(result, 'TrainingStyle') && self.filterBySingleFacet(result, 'Services')) {
				self.$scope.search.filteredResults.push(result);
				self.selectRate(result);
			}
		});
	};

	private updateResultCounts = () => {
		let $scope = this.$scope;

		$scope.search.totalResultsCount = $scope.search.filteredResults.length;
		$scope.search.shownResultsCount = Math.min($scope.search.pageSize, ($scope.search.totalResultsCount - (($scope.search.currentPage - 1) * $scope.search.pageSize)));
	};

	private filterBySingleFacet = (result, filter) => {
		var valid = false;

		// always match if no filter is set
		if ($scope.search.activeFilters[filter].length == 0) {
			return true;
		}

		angular.forEach($scope.search.activeFilters[filter], function (value) {
			if (_.find(result[filter], function (o) {
				return o == value.Key || o.Key == value.Key;
			}) != undefined) {
				valid = true;
			}
		});

		return valid;
	};

	private consultationOpen = () => {
		this.$rootScope.$broadcast('consultationOpen');
	}

	private isSearching = () => {
		this.$rootScope.$broadcast('isSearching', true);
	}
	private messageOpen = (event: ng.IAngularEvent, trainer) => {
		event.preventDefault();
		event.stopPropagation();

        this.$rootScope.$broadcast('messageOpen', trainer);
	}
}
